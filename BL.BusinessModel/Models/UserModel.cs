﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DL.DomainModel.Entities;

namespace BL.BusinessModel.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public int? RoleId { get; set; }
        public Role Role { get; set; }
    }
}
