﻿using System.Collections.Generic;
using BL.BusinessModel.Models;
namespace BL.Interfaces.Services
{
    public interface IUserService
    {
        bool Login(UserModel model);
        void Register(UserModel model);

        UserModel GetUserById(int id);

        UserModel GetUserByEmail(string email);

        bool UpdateUser(UserModel model);

        ICollection<UserModel> GetAllUsers();

        bool DeleteUserByEmail(string email);
        bool UserExists(string email);
    }
}