﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DL.DomainModel.Entities;
using Microsoft.EntityFrameworkCore;

namespace DL.Interfaces.DatabaseContext
{
   public interface IAppDbContext: IDbContext
    {
        DbSet<User> Users { get; set; }
        DbSet<Role> Roles { get; set; }
    }
}
