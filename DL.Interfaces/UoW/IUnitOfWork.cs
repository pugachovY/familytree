﻿using System.Collections.Generic;
using DL.DomainModel.Base;

namespace DL.Interfaces.UoW
{
    public interface IUnitOfWork
    {
        TEntity Add<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;
        void Add<TEntity>(ICollection<TEntity> entities) where TEntity : class, IBaseEntity;
        void Update<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;
        void Delete<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;
        void Delete<TEntity>(ICollection<TEntity> entities) where TEntity : class, IBaseEntity;
        void SaveChanges();
    }
}