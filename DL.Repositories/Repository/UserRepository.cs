﻿using System.Collections.Generic;
using System.Linq;
using DL.DomainModel.Entities;
using DL.Interfaces.Repository;
using DL.DatabaseContext.Context;
using Microsoft.EntityFrameworkCore;

namespace DL.Repositories.Repository
{
    public class UserRepository:IUserRepository
    {
        private readonly AppDbContext _appDbContext;

        public UserRepository(AppDbContext dbContext)
        {
            _appDbContext = dbContext;
        }
        public User GetUserById(int id)
        {
            return _appDbContext.Users.AsNoTracking().SingleOrDefault(u => u.Id == id);
        }

        public User GetUserByEmail(string email)
        {
            return _appDbContext.Users.AsNoTracking().SingleOrDefault(u => u.Email == email);
        }

        public ICollection<User> GetAllUsers()
        {
            return _appDbContext.Users.AsNoTracking().ToList();
        }

        public bool CheckExistUser(string email)
        {
            return _appDbContext.Users.Select(u=> u.Email).AsNoTracking().Contains(email);
        }

        public User GetRegistredUser(string email, string password)
        {
            return _appDbContext.Users.AsNoTracking().SingleOrDefault(u => u.Email==email && u.Password==password);
        }
    }
}