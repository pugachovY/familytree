﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using BL.BusinessModel.Models;
using BL.Interfaces.Services;
using DL.DomainModel.Entities;
using DL.Interfaces.DatabaseContext;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IAppDbContext _appDbContext;

        public AccountController(IUserService userService, IMapper mapper, IAppDbContext appDbContext)
        {
            _userService = userService;
            _mapper = mapper;
            _appDbContext = appDbContext;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            var model = new LoginViewModel();
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Login(LoginViewModel model)
        {
           
            if (ModelState.IsValid)
            {
                User userM = _appDbContext.Users
                   .Include(u => u.Role)
                   .FirstOrDefaultAsync(u => u.Email == model.Email && u.Password == model.Password).Result;
                var user = _mapper.Map<UserModel>(userM);
                var isLogin = _userService.Login(user);
                if (isLogin)
                {
                    Authenticate(user); 

                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("LoginError", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register()
        {
            var model = new RegisterViewModel();
            return View(model);
        }
        [HttpPost]
        [AllowAnonymous]
        public IActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _mapper.Map<UserModel>(model);
                var isExist = _userService.UserExists(user.Email);
                if (isExist)
                {
                    ModelState.AddModelError("", "Пользователь с таким Email уже сеществует");
                }
                else
                {
                    Role userRole = _appDbContext.Roles.FirstOrDefaultAsync(r => r.Name == "user").Result;
                    if (userRole != null)
                    {
                        //user.Role = userRole;
                        user.RoleId = userRole.Id;
                    }
                    _userService.Register(user);
                    return RedirectToAction("SuccessRegistration");
                }
            }
            return View(model);
        }
        public ActionResult SuccessRegistration()
        {
            return View();
        }
        private void Authenticate(UserModel user)
        {
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role?.Name)
            };
            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки
            HttpContext.Authentication.SignInAsync("Cookies", new ClaimsPrincipal(id));
        }

        public IActionResult Logout()
        {
            HttpContext.Authentication.SignOutAsync("Cookies");
            return RedirectToAction("Login", "Account");
        }
    }
}
