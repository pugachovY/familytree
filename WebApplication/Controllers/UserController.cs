﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BL.BusinessModel.Models;
using BL.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    [Authorize]
    public class UserController:Controller
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public UserController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            var users =
            _userService.GetAllUsers();
            return View(users);
        }
        public IActionResult Profile()
        {
            var user = _mapper.Map<UserViewModel>(_userService.GetUserByEmail(User.Identity.Name));
            return View(user);
        }

        [HttpGet]
        public IActionResult Edit()
        {
            var userModel = _mapper.Map<UserViewModel>(_userService.GetUserByEmail(User.Identity.Name));
            return View(userModel);
        }
        
        [HttpPost]
        public IActionResult Edit(UserViewModel model)
        {
            
            var user = _mapper.Map<UserModel>(model);
            var result = _userService.UpdateUser(user);
            if (result)
            {
                ViewBag.Edit = "Редактирование прошло успешно";
            }
            else
            {
                ViewBag.Edit = "Ошибка";
            }
            return View(model);
        }
    }
}
